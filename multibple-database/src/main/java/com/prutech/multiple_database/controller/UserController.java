package com.prutech.multiple_database.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prutech.multiple_database.book.model.Book;
import com.prutech.multiple_database.book.repo.BookRepo;
import com.prutech.multiple_database.user.model.User;
import com.prutech.multiple_database.user.repo.UserRepo;

@RequestMapping("/multiple")
@RestController
public class UserController {

	@Autowired
	private BookRepo bookRepo;
	
	@Autowired
	private UserRepo userRepo;
	
	@RequestMapping(value = "/save")
	public boolean saveUsers() {
		
		userRepo.save(new User("one","chandu"));
		bookRepo.save(new Book("one","Java"));
		return true;

	}
	
	@GetMapping(value = "/getusers")
	public List<User> getUsers(){
		
		return userRepo.findAll();
	}

	@GetMapping(value = "/getbooks")
	public List<Book> getBooks(){
		
		return bookRepo.findAll();
	}


}

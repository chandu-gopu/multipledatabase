package com.prutech.multiple_database.book.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prutech.multiple_database.book.model.Book;

@Repository
public interface BookRepo extends JpaRepository<Book, String>{

}

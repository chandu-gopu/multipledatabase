package com.prutech.multiple_database.user.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prutech.multiple_database.user.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, String> {

}

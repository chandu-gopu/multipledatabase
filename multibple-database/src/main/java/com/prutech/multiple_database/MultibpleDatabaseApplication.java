package com.prutech.multiple_database;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.prutech.multiple_database" })
public class MultibpleDatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultibpleDatabaseApplication.class, args);
	}

}
